\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Time-dependent Hartree-Fock calculations}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Initial states -- imaginary time propagation}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Monopole compression mode}{4}{subsection.2.2}
\contentsline {paragraph}{In-phase oscillations}{6}{paragraph*.1}
\contentsline {paragraph}{Out-of-phase oscillations}{7}{paragraph*.2}
\contentsline {subsection}{\numberline {2.3}Radial modes}{8}{subsection.2.3}
\contentsline {section}{\numberline {3}Comparison to hydrodynamic approach}{10}{section.3}
\contentsline {section}{\numberline {4}Conclusions}{12}{section.4}
\contentsline {paragraph}{Funding information}{12}{paragraph*.3}
\contentsline {section}{References}{12}{paragraph*.3}
