\documentclass[submission, Phys]{SciPost}
\usepackage[T1]{fontenc}
\usepackage{pstricks} 
%\usepackage{subfigure}
\usepackage[normalem]{ulem}
\usepackage{graphicx} 
%\usepackage{bm}       
\usepackage{amsbsy}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{fixmath}
%\usepackage[colorlinks]{hyperref}
\usepackage{physics}
%\usepackage{bbm}
%\usepackage{bm}
\usepackage{color}
\usepackage{lipsum}
%\usepackage{multirow}
%\usepackage{tikz}

\hypersetup{
	colorlinks = true,
	bookmarksnumbered,
	pdfstartview={FitH},
	citecolor={darkgreen},
	linkcolor={scipostdeepblue},
	urlcolor={scipostdeepblue},
	pdfpagemode={UseOutlines}}
\definecolor{darkgreen}{RGB}{20,100,20}
\definecolor{darkblue}{RGB}{0,0,130}
\definecolor{darkred}{rgb}{.8,0,0}

\newcommand{\nn}{\nonumber}
\providecommand*{\I}{\mathrm{i}} 
%\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\bl}[1]{\textcolor{blue}{\textbf{#1}}}
\renewcommand{\vec}[1]{\mathbold{#1}}
\renewcommand{\d}{\mathrm{d}}
\newcommand{\spinor}[3]{\left(\hspace{-1.0ex}\begin{array}{c}#1\\ #2\end{array}\hspace{-1.0ex}\right)_{\hspace{-0.7ex}#3}\hspace{-2.0ex}}
\newcommand{\nab}{\mathbold{\nabla}}



\begin{document}


\begin{center}{\Large \textbf{
Collective oscillations of a two-component Fermi gas on the repulsive branch
}}\end{center}

% TODO: write the author list here. Use initials + surname format.
% Separate subsequent authors by a comma, omit comma at the end of the list.
% Mark the corresponding author with a superscript *.
\begin{center}
T. Karpiuk\textsuperscript{1},
P. T. Grochowski\textsuperscript{2*},
M. Brewczyk \textsuperscript{1}
K. Rz\k{a}{\.z}ewski\textsuperscript{2}
\end{center}

\begin{center}
{\bf 1} Wydzia{\l} Fizyki, Uniwersytet w Bia{\l}ymstoku,  ul. K. Cio{\l}kowskiego 1L, 15-245 Bia{\l}ystok, Poland
\\
{\bf 2} Center for Theoretical Physics, Polish Academy of Sciences, Aleja Lotnik\'ow 32/46, 02-668 Warsaw, Poland
\\
* piotr@cft.edu.pl
\end{center}

\begin{center}
\today
\end{center}

% For convenience during refereeing: line numbers
%\linenumbers

\section*{Abstract}
{\bf
We calculate frequencies of collective oscillations of two-component Fermi gas that is kept on the repulsive branch of its energy spectrum.
Not only is a paramagnetic phase explored, but also a ferromagnetically separated one.
Both in-, and out-of-phase perturbations are investigated, showing contributions from various gas excitations.
Additionally, we compare results coming from both time-dependent Hartree-Fock and density-functional approaches. 
}


% TODO: include a table of contents (optional)
% Guideline: if your paper is longer that 6 pages, include a TOC
% To remove the TOC, simply cut the following block
\vspace{10pt}
\noindent\rule{\textwidth}{1pt}
\tableofcontents\thispagestyle{fancy}
\noindent\rule{\textwidth}{1pt}
\vspace{10pt}

\section{Introduction}
Theoretical and experimental studies of multi-component quantum mixtures have always played a crucial role in our attempts to understand the quantum theory~\cite{Pethick2008,Pitaevskii2016}.
For several decades, the central point of such considerations was reserved for mixtures of different states of helium~\cite{Ebner1971}.
However, the first realizations of quantum degenerate gases in 1990s~\cite{Anderson1995,Davis1995,Bradley1995} have created a playground for scientists that not only allows one to combine different fermionic and bosonic ingredients of the mixture, but also to freely tune the interatomic interactions by the means of Feshbach resonances~\cite{Chin2010}.

One of the main techniques to study the excitations of a trapped, interacting quantum mixture is to examine its collective oscillations~\cite{Mewes1996,Jin1996}.
Depending of the particular scenario of an experimental excitation procedure, such oscillations can be classified into different categories~\cite{Pitaevskii2016}.
Firstly, if the sample does not undergo a change in the volume, but its geometric shape is altered, the mode is called a surface one.
Such modes were employed to study a transition from collisionless to hydrodynamic regimes in both bosonic~\cite{Stamper-Kurn1998,Buggle2005} and fermionic~\cite{Altmeyer2007,Wright2007} gases.
On the other hand, if a volume change happens, one deals with a so called breathing or compression mode~\cite{Stringari1996,Chevy2002}.
Those types of excitations have proved useful in the investigation of equations of state for strongly interacting fermionic gases~\cite{Kinast2004,Bartenstein2004,Altmeyer2007a}.

Both theoretical and experimental considerations~\cite{Busch1997,Esry1998,Ho1998,Hall1998a,Bijlsma2000,Capuzzi2001,Yip2001,Goral2002,Pu2002,Svidzinsky2003,Liu2003,Deconinck2004,Rodriguez2004,Navarro2009} showed that collective oscillations in multi-component mixtures are affected by a variety of different effects, among the others, damping and frequency shifts~\cite{Vichi1999,Maddaloni2000,Gensemer2001}.
Recently, much of the focus was centered onto center-of-mass oscillations (or spin-dipole) that have been employed to study coupling effects in mixtures of distinctive superfluids~\cite{Ferrier-Barbut2014,Delehaye2015,Roy2017,Wu2018} and fermion-mediated bosonic interactions~\cite{DeSalvo2019}.

The richness of effects increases even more when decays through different channels and collapses~\cite{Modugno2002,Ospelkaus2006} or phase separations~\cite{Ospelkaus2006a,Papp2008,Shin2008,Lous2018} come into play.
Recently, oscillations in repulsive Fermi-Fermi~\cite{Sommer2011,Valtolina2016} and Bose-Fermi~\cite{Huang2019} mixtures in the presence of phase-separating domain wall were studied experimentally.
We focus on the former case, in which phase separation can be interpreted as a manifestation of the long-standing problem of ferromagnetic (Stoner) instability~\cite{Stoner1933,Jo2009,Cui2010,Pilati2010,Massignan2011,Chang2011,Pekker2011,Sommer2011,Sanner2012,Massignan2014,Trappe2016,Valtolina2016,Amico2018}.

In such a system, two equally populated spin species (or in experimental terms -- two hyperfine states) of Fermi gas interact only via repulsive short-range potential.
However, the ground state of such a mixture is a superfluid of paired atoms of opposite spins (so called lower or attractive branch of the energy spectrum), rather than a ferromagnet (upper or repulsive branch)~\cite{Sanner2012}.
It stems from the fact that true zero-range interactions tuned by Feshbach resonances necessarily need an underlying attractive potential with a weakly bound molecular state~\cite{Chin2010}.

Due to phase-separated state being intrinsically unstable towards decay into such molecules, early experiments dealt with high rates of losses~\cite{Jo2009}.
To overcome these problems, in recent experiments artificial domain structure was initially prepared, making the phase-separated state stable for a finite time~\cite{Sommer2011,Valtolina2016}.

As for now, theoretical studies have skipped analysis of small oscillations in this system while excited onto repulsive branch, instead focusing mainly on the attractive one~\cite{Vichi1999,Bruun2001,Maruyama2006,Maruyama2007,DeSilva2005}.
In this work, we fill this void by considering in- and out-of-phase radial and breathing modes of purely repulsive two-component fermionic mixture for both overlapping and phase-separated states.
We compare our results with the previously available weakly-interacting ones and show how to refine them by means of the renormalization of the interaction. 
Moreover, we compare the results obtained by the means of time-dependent Hartree-Fock calculations with the hydrodynamic approach~\cite{Grochowski2017a}, pointing out the regimes of applicability of the latter method.

The paper is structured as follows.
In Sec.~\ref{tdhf}, we analyze excitation frequencies by the means of time-dependent Hartree-Fock methods and compare them to past theoretical techniques.
Sec.~\ref{hyd} is dedicated to analysis of applicability of hydrodynamic methods in similar by comparing such results to atomic orbital calculations.
In the final Sec.~\ref{conc}, we briefly review obtained results and provide suggestions for further work.

\section{Time-dependent Hartree-Fock calculations}\label{tdhf}
We start our considerations by describing the method used to analyze the statics and dynamics of the mixture.
We employ time-dependent Hartree-Fock (or atomic-orbital) approach~\cite{Gawryluk2017,Grochowski2017a} in which we assume that many-body wave function  $\Psi({\bf x}_1^{},...,{\bf x}_{N})$ of $N$ indistinguishable fermionic atoms is given by a single Slater determinant:
\begin{eqnarray}
&&\Psi ({\bf x}_1,...,{\bf x}_{N}) 
= \frac{1}{\sqrt{N!}} \left |
\begin{array}{lllll}
\varphi_1({\bf x}_1) & . & . & . & \varphi_1({\bf x}_{N}) \\
\phantom{aa}. &  &  &  & \phantom{aa}. \\
\phantom{aa}. &  &  &  & \phantom{aa}. \\
\phantom{aa}. &  &  &  & \phantom{aa}. \\
\varphi_{N}({\bf x}_1) & . & . & . & \varphi_{N}({\bf x}_{N})
\end{array}
\right |  .   \nonumber  \\
\label{Slater}
\end{eqnarray}
The coordinates ${\bf x}_i$ of an atom comprise both spatial and spin variables and $\varphi_i({\bf x}),\, {i=1,...,N}$ denote different, orthonormal spin-orbitals.
As in scenario that we describe both spin states are equally populated, we further assume that the spin-dependent part of spin-orbitals is twofold and that exactly half of the atoms occupy each spin state. 

We restrict ourselves to only contact interaction between different species, as such a description is very well accurate for systems with realistic short-range potentials (e.g. coming from broad Feshbach resonances).
Such an interaction can be described by a single parameter, $s$-wave scattering length $a$, which can be connected to the coupling constant ${g\ge0}$ by ${g=4\pi\,a\hbar^2/m}$.
Therefore, the time-dependent Hartree-Fock equations for the spatial parts of the spin-orbitals, i.e. spatial orbitals representing the first component, $\varphi_{i,+} ({\bf r},t)$, and the second component, $\varphi_{i,-} ({\bf r},t)$, are given by
\begin{eqnarray}
&& i\hbar \partial_t  \varphi_{i,\pm} ({\bf r},t) =
( -\frac{\hbar^2}{2 m} \nabla^2 + V_{tr}({\bf r})  
+ g n_{\mp} ({\bf r},t) \; ) \; \varphi_{i,\pm} ({\bf r},t) \nonumber \\
\label{HFeq}
\end{eqnarray}
for $i=1,...,N/2$ and with
\begin{eqnarray}
n_{\pm} ({\bf r},t) = \sum_{j=1}^{N/2} |\varphi_{j,\pm} ({\bf r},t)|^2   \,.
\end{eqnarray}   
Here, $m$ is the mass of fermionic atom, $V_{tr}({\bf r})$ is the trapping potential taken to be a radially symmetric harmonic trap, $V_{tr}({\bf r})=\frac{1}{2} m \omega^2 r^2$, and $n_{\pm}$ are single-particle densities of both species.

To further refine the interspecies interaction and include many-body quantum corrections, we locally renormalize the coupling strength by replacing the bare scattering length, $a$, by the effective (and symmetrized) one: 
\begin{equation}
a_{eff} = [\zeta{(k_+ a)}/{k_+} + \zeta{(k_- a)}/{k_-} ] /2,
\end{equation}
where $k_+({\bf r})$ and $k_-({\bf r})$ are the local Fermi momenta for the first and the second component, respectively, and $\zeta{(\tilde{k}_F a)}$ is the renormalization function \cite{VonStecher2007,Grochowski2017a}.
The renormalization function $\zeta{(\tilde{k}_F a)}$ is expanded perturbatively into powers of $\tilde{k}_F a$: 
\begin{equation}
\zeta{(\tilde{k}_F a)} = \tilde{k}_F a + B (\tilde{k}_F a)^2 + D (\tilde{k}_F a)^3 + ... .
\end{equation}

For the physical interpretation and numerical values of consecutive perturbative terms, see e.g.~\cite{Grochowski2017a}.
Taking only first-order terms in the above expansion results in mean-field Eqs. (\ref{HFeq}).
However, taking into account the second- and third-order terms changes the time-dependent Hartree-Fock Eqs. (\ref{HFeq}) in the following way:
\begin{align}
g n_{\pm} \rightarrow g n_{\pm} + & C (4/3\, n_{\mp}^{1/3}\, n_{\pm} + n_{\pm}^{4/3} ) + \nonumber \\
& E (5/3\, n_{\mp}^{2/3}\, n_{\pm} + n_{\pm}^{5/3} ),
\end{align} 
where $C=ga B\, (6\pi^2)^{1/3}/2$, $E=g a^2 D\, (6\pi^2)^{2/3}/2$. 

The HF equations can describe both statics and dynamics of the mixture -- the usual method is employment of the split-step operator to propagate the equations in time~\cite{Gawryluk2017}.
To obtain a ground state, this time is imaginary, and for the dynamics -- real.

\subsection{Initial states -- imaginary time propagation}
The usual number of atoms we use in our calculations is $N=56+56$.
It is far below the experimental values, however the behavior of a considered mixture is universal\footnote{The word \textit{universal} relates to the behavior of the observables we consider -- the collective frequencies and the interaction strength at which the phase separation happens; On the other hand, e.g. density profiles differ, slowly approaching Thomas-Fermi approximation with the growing number of atoms.} when described in terms of dimensionless interaction parameter, $k_Fa$.
$k_F$ is the Fermi wave number in the center of a trap in Thomas-Fermi approximation, ${k_F=(24 N)^{1/6}/a_{HO}}$, and $a_{HO}$ is the harmonic oscillator length.
The number of atoms we consider proves to be more than enough to be in this universal regime.
Moreover, we confirm this assumption by performing single calculations with larger numbers of atoms.

In Fig.~\ref{l1} we present the results of propagation of the HF equations in imaginary time, yielding ground states of the mixture for given interactions.
We can see that for small interactions the clouds fully overlap (gas stays fully unpolarized), but become larger as the interaction increases.
At some critical interaction strength, $k_F a_c$, phase separation occurs.
For the interaction only slightly above the critical one, this separation happens only in the middle of the trap -- the gas on the perimeter of the cloud stays unpolarized.
What is more, the surface at which the separation occurs differs from one numerical realization to another -- there is no privileged direction in which the splitting can occur.
To ensure that the domain wall manifests perpendicularly to the $z$ axis, we introduce very small additional linear potential in $z$ direction that assists with the symmetry breaking.
This linear potential is of opposite direction for different species.

For a very strong interaction, there is no longer any appreciable unpolarized gas on the perimeter and the clouds have vanishingly small overlap.
In Fig.~\ref{l1} each of the scenarios is presented with a single particle density along the $z$ axis.
The single density profile in other directions preserves cylindrical symmetry.

In order to get value of $k_F a_c$ consistent with both more sophisticated theoretical approaches (Quantum Monte Carlo, LOCV, large $N$ expansion) and experimental results, we renormalize perturbatively the scattering length in the way described above.
In the trap, this critical value is $k_F a_c \approx 0.9$.
\begin{figure}[htbp!]
	\centering
	\includegraphics[width=0.7\linewidth]{dens.pdf}
	\caption{Ground-state densities for different values of interaction strength.
		For weak interaction, clouds stay unpolarized, but for some critical value of repulsion, they enter the ferromagnetic phase.
		Initially, just above the transition, gas becomes partially polarized only in the center of the trap, being unpolarized on the perimeter.
		For strong enough interaction, the overlapping part vanishes, leaving the clouds fully separated.
		\label{l1}}
\end{figure}

\subsection{Monopole compression mode}
We start with the case of monopole compression mode.
It is usually performed by symmetrically perturbing both clouds, by either slightly expanding or squeezing the trap, exciting oscillations with radial symmetry preserved.
The gas changes its volume, therefore this mode is classified as a compression one.
The frequency of oscillations is taken from analysis of the cloud's width in a radial or axial direction.

Firstly, we compare previous theoretical approaches in a weakly interacting regime without a renormalization of the interaction strength.
From here onwards, we will use atomic units, $\hbar = \omega = m = 1$.
The starting point is a sum rule approach, firstly used for the oscillations in spin half Fermi gas by Vichi and Stringari~\cite{Vichi1999}.
The expression for a monopole mode in a harmonic trap reads
\begin{align}
\omega_R=\sqrt{3+\frac{ E_{\text{kin}}+3 E_{\text{int}}}{E_{\text{tr}}}}.
\end{align}

We are interested in weakly interacting case, for which phase separation does not occur, and Thomas-Fermi ground-state densities provide a reliable approximation.
Under the assumption that spherical symmetry is not broken and both clouds fully overlap, $n(\vec{r})=n(r)=n_{+}(r)=n_{-}(r)$, consecutive mean field energies read
\begin{align}
& E_{\text{kin}} = \frac{2^{8/3} 3^{5/3} \pi^{7/3}}{5}\int n^{5/3} (r) r^2 \dd r \nonumber \\
& E_{\text{int}} = 4 \pi g\int n^{2} (r) r^2 \dd r \nonumber \\
& E_{\text{ho}} = 4 \pi \int n (r) r^4 \dd r.
\end{align}

In Ref.~\cite{Vichi1999}, noninteracting Thomas-Fermi profiles
\begin{align}
n(r) = \left( \frac{(3 N)^{1/3}-\frac{1}{2} r^2}{A} \right)^{3/2}
\end{align}
are used, where $A=\frac{6^{5/3} \pi^{4/3}}{12}$.
The result is then quite easily calculated analytically, yielding simple expression 
\begin{align}
\omega_R \approx 2 \sqrt{1+0.11 k_F a}.
\end{align}
This result is shown in Fig.~\ref{comp} as a blue line.

\begin{figure}[htbp!]
	\centering
	\includegraphics[width=0.7\linewidth]{comp.pdf}
	\caption{Frequency of a monopole mode calculated with the help of four different methods within the nonrenormalized mean field regime: sum rule approach from Ref.~\cite{Vichi1999} using noninteracting and interacting Thomas-Fermi ground-state density profiles, linearized hydrodynamic equations and time-dependent Hartree-Fock equations.
		We can see that the latter three give very similar results, while the first on greatly overestimates the frequencies.
		\label{comp}}
\end{figure}

However, one can refine this result by considering ground-states profiles that come from Thomas-Fermi equations with mean-field interactions included (see Eq. 12 in Ref.~\cite{Trappe2016}):
\begin{align}
A n^{2/3} + g n = \mu - \frac{1}{2} r^2,
\end{align}
where $\mu$ is a global chemical potential, that is implicitly given by the number of atoms through normalization procedure.
This equation can be readily solved (see Appendix in Ref.~\cite{Trappe2016}) by means of e.g. Cardano formulae, yielding ground state profiles that have analytical, however quite complicated form.
Using these density profiles, one gets an orange line in Fig.~\ref{comp}. 

Apart from curves from sum rule approach, there are two more results presented in Fig.~\ref{comp}: oscillation frequencies extracted from analysis of time evolution of TDHF equations, and frequencies from linearization of hydrodynamic formulation of the problem (further discussed in Sec.~\ref{hyd}).
One can see that all of them give very similar results for this particular mode.

For the further analysis, we decide to use TDHF approach over other two for the following reasons.
Firstly, sum rule approach is not well suited to account for a system that undergoes phase separation and moreover has to be provided with ground-state profiles from external theory.
As for the hydrodynamic approach, it can be readily utilized for both statics and dynamics of considered system, however it is not obvious for which situation the system really behaves hydrodynamically.
On the other hand, atomic orbital approach does not differentiate between collisionless and collisionally hydrodynamic regimes, as it should work well in both of them.
The connection between hydrodynamic and atomic orbital methods will be further elaborated in Sec.~\ref{hyd}.

\paragraph{In-phase oscillations}
Firstly, we analyze radial oscillations with fully renormalized interaction for both weakly and strongly interacting gas.
Fig. \ref{in_phase_radial} provides the results. 
Inclusion of the higher order terms in the interaction shifts the phase transition towards smaller values of $k_F a$ and increases the maximum value of the oscillation frequency, which is situated just before the transition occurs.
It is now ca. $\omega/\omega_0 \approx 2.2$ in comparison to $\omega/\omega_0 \approx 2.1$ for the bare mean-field model.

After the transition, again there is only one dominating frequency that decreases with growing interacting and decreasing overlap of the clouds, achieving noninteracting value  $\omega/\omega_0 \approx 2.0$ for a very strong repulsion.
It suggests that in the fully separated regime, the domain wall has no effect on the frequency of the radial oscillations as compared to the noninteracting, polarized gas of $N$ fermionic atoms.
This is the only case in which the oscillations can be fitted into a sum of sines for the interaction above the critical value.
For all the other cases, the analysis is based only on the Fourier transform for each interaction strength.

\begin{figure}[htbp!]
	\centering
	\includegraphics[width=0.7\linewidth]{rad-in.pdf}
	\caption{  Frequency of a monopole mode for in-phase perturbation of both fermionic species for varying interaction strength.
		The critical interaction for which the phase separation occurs reads $k_F a \approx 0.9$.
		In each of Figs. 3-8 that show consecutive type of oscillations, we present two results.
		Firstly, filled markers (circles, squares, triangles) are associated with the frequencies that can be recovered from the fit to a sum of sines. 
		Such a fit cannot be meaningfully performed for every interaction regime, especially after the transitions, therefore we additionally present Fourier transform of the oscillation run for each of the interaction values.
		This transform is normalized in such a way that for every interaction strength, the maximum value of transform's absolute value is equaled to $1$.
		For better visibility, we decide to plot not the absolute value of the FT, but rather FT brought to power $1/3$.
		\label{in_phase_radial}}
\end{figure}
\begin{figure}[htbp!]
	\centering
	\includegraphics[width=0.7\linewidth]{rad-out.pdf}
	\caption{  Frequency of a monopole mode for out-of-phase perturbation of both fermionic species for varying interaction strength.
		\label{out_of_phase_radial}}
\end{figure}

\paragraph{Out-of-phase oscillations}
Additionally, we consider out-of-phase radial oscillations.
It is achieved by perturbing both clouds differently -- when one of them undergoes squeezing of the trap, the other feels the trap to be widening.
To have clean out-of-phase oscillations, the small deviations of the densities ought to behave like $\delta n_+ = - \delta n_-$.
However, it is nontrivial how to achieve such deviations as such a linear behavior needs infinitesimal perturbation and as a result, usual experimental setting mixes both in- and out-of-phase types of excitations.
A potential experimental realization of such a scenario could involve imposing magnetic trapping on top of the optical one.
The magnetic field would repel one of the species from the center of the trap, and attract the other one.
Moreover, it is not unthinkable to find such a out-of-phase contribution in some other excitation schemes (e.g. quenching from one value interaction to another).

The results are presented in Fig. \ref{out_of_phase_radial}.
Below the critical value of the interaction, two branches are clearly visible.
The upper one constitutes a contribution from in-phase oscillations that are excited by this particular scheme.
The lower branch consists of out-of-phase contribution.
Contrary to the upper one, the frequency decreases with the interaction, achieving ca. $\omega / \omega_0 \approx 1.2$ just before the phase transition.
The Fourier transform shown as a background confirms that the lower branch is much stronger, with only small part of oscillations excited on the upper one.

After the transition, oscillations tend not to have a clear decomposition into small finite number of frequencies.
However, the Fourier transforms of each of the time evolutions show strong contributions from the frequencies that can be considered extensions of two branches from the paramagnetic phase.
The upper branch follows the trend from the in-phase perturbation scheme, decreasing and achieving nonincteracting value.
The same thing happens for the lower branch -- it increases and goes to  $\omega / \omega_0 \approx 2.0$ for very strong interaction.
Contrary to the pre-transition regime, upper branch appears to be excited stronger.
Additionally, contributions from other, lower- and higher-lying frequencies are clearly visible. 

\subsection{Radial modes}
The next excitation that we analyze is a radial compression (breathing) mode.
The perturbing scheme involves slightly compressing the gas in two out of three directions, leaving one of the axes unperturbed.
Again, we perform in- (Fig.~\ref{in_phase_quad_rad}) and out-of-phase (Fig.~\ref{out_of_phase_quad_rad}) calculations.

As for the in-phase case, we again can distinguish between two branches before the transition, however this time they are equally strong.
The upper branch again achieves $\omega / \omega_0 \approx 2.2$ near the transition, but the lower one falls down to only $\omega / \omega_0 \approx 1.8$, giving rise to another distinctive excitation.
After the transition, upper branch appears to be clearly more dominating, falling to noninteracting value for strongly interacting gas.

The out-of-phase case exhibits three distinctive branches -- the upper ($\omega / \omega_0 \approx 2.2$ near the transition), the middle ($\omega / \omega_0 \approx 1.8$) and the lower ($\omega / \omega_0 \approx 1.2$) one.
The latter clearly dominates.
Again, after the transition, each of them is visible, going back to noninteracting regime, with the upper one staying the most pronounced.
\begin{figure}[htbp!]
	\centering
	\includegraphics[width=0.7\linewidth]{quad-rad-in-v2.pdf}
	\caption{  Frequency of a radial compression mode for in-phase perturbation of both fermionic species for varying interaction strength.
		\label{in_phase_quad_rad}}
\end{figure}
\begin{figure}[htbp!]
	\centering
	\includegraphics[width=0.7\linewidth]{quad-rad-out_v2.pdf}
	\caption{   Frequency of a radial compression mode for out-of-phase perturbation of both fermionic species for varying interaction strength.
		\label{out_of_phase_quad_rad}}
\end{figure}

The last excitation we consider is a type of a surface mode, called a radial quadrupole mode, in which gas is again excited only in two axes, however alternately, giving rise only to change of the shape of the clouds, but not their volume.
The results for in-phase perturbing scheme are presented in Fig.~\ref{in_phase_quad_xy} and for the out-of-phase one in Fig.~\ref{out_of_phase_quad_xy}.

The in-phase excitation shows two distinctive branches -- the usual upper one, and the lower one that near the transition achieves $\omega / \omega_0 \approx 1.8$.
Contrary to previous cases, the lower branch stays stronger than the upper one after the transition.
The out-of-phase case is characterized by the same previous three branches, with the lowest being the most dominating.
This time, the upper branch stays the most pronounced over the transition.

\begin{figure}[htbp!]
	\centering
	\includegraphics[width=0.7\linewidth]{quad-xy-in.pdf}
	\caption{ Frequency of radial quadrupole mode for in-phase perturbation of both fermionic species for varying interaction strength.
		\label{in_phase_quad_xy}}
\end{figure}

\begin{figure}[htbp!]
	\centering
	\includegraphics[width=0.7\linewidth]{quad-xy-out-v2.pdf}
	\caption{  Frequency of radial quadrupole mode for out-of-phase perturbation of both fermionic species for varying interaction strength.
		\label{out_of_phase_quad_xy}}
\end{figure}


\section{Comparison to hydrodynamic approach}\label{hyd}
We now proceed to briefly compare the atomic orbital approach and the hydrodynamic (in this case sometimes called time-dependent Thomas-Fermi~\cite{Domps1998}) one.
The most important assumption that underlies a hydrodynamic behavior is a fast relaxation of any dynamical distortions in momentum distribution towards a local Fermi sphere centered around local hydrodynamic momentum of the collective flow.
Such a thermalization is expected when both clouds overlap and interact by two-body collisions, but is not present in collisionless, noninteracting regime that governs the nonoverlapping parts of the clouds~\footnote{However, some works suggested that Pauli principle can act as a substitute to interactions, and the hydrodynamic description can be working even for very weakly interacting Fermi gas~\cite{Karpiuk2002}.}.
However, previous studies showed that such a hydrodynamic description works well for a spin-dipole mode for which experimental results are retrieved even in a weakly interacting regime.
It is partially due to the fact, that the noninteracting frequency of this particular oscillation coincides with the hydrodynamic one.
Then, corrections due to the interaction (even the weak one) seem to stem from the overlapping parts of the clouds, and can be described by the hydrodynamics.
It is not obvious for which type of oscillations considered in this work hydrodynamic description can be used.
We try to answer this question by comparing TDHF and hydrodynamic results.

The starting point for hydrodynamic methods is the density functional approach introduced for such a repulsive mixture in Ref.~\cite{Trappe2016}.
In this theory, the full Hamiltonian is given by $H=T_{\text{tot}}+E_{\text{int}}+E_{\text{pot}}$, and each of the terms can be written as a functional of one-particle density, making use of local density approximation.
The first term, the total kinetic energy ${T_{\text{tot}}=T+T_{\text{c}}}$ consists of the intrinsic kinetic energy $T$, which is approximated by the Thomas-Fermi functional, $T=\sum_{j=\pm}\int\d\vec{r}\ \frac{3}{5}A n_j^{5/3}$ , and the kinetic energy of the collective motion, ${T_{\text{c}}=\sum_{j=\pm}\int\d\vec{r}\  \frac{m}{2}n_j\,\vec v_j^2}$.
The potential energy is takes the form $E_{\text{pot}}=\sum_{j=\pm}\int\d\vec{r} \  V_j n_j$.
%Additionally, zero-curl velocity fields ${\vec v_\pm=\nab\chi_\pm}$ are assumed.
The contact interaction term is given as an overlap between the density profiles of the components, $E_{\text{int}}=g \int  \d {\vec{r}} \, n_+ \, n_-$.

To connect this DFT description to hydrodynamics, classical pseudo-wavefunction in the spirit of early Madelung's works~\cite{Madelung1927} is introduced:
\begin{align}\label{TwoCompPsi}
\psi=\spinor{\psi_+}{\psi_-}{\phantom{\Delta\tau}}=\spinor{\sqrt{n_+}\,e^{\I\frac{m}{\hbar}\chi_+}}{\sqrt{n_-}\,e^{\I\frac{m}{\hbar}\chi_-}}{\phantom{\Delta\tau}},
\end{align}
where ${n_++n_-=\psi^\dagger\psi}$ retrieves one-particle density, and ${\nab\chi_\pm=\vec v_\pm}$ are the irrotational velocity fields of the collective motion.
The Euler-Lagrange equation for these fields under the Hamiltonian $H$ then take hydrodynamic form:
\begin{align}\label{TwoCompMadelungHydro}
\begin{split}
\partial_t n_\pm&=-\nab(n_\pm\,\vec v_\pm),\\
m\partial_t\vec v_\pm&=-\nab\left(\frac{\delta T}{\delta n_\pm}+\frac{m}{2}\vec v_\pm^2+V_\pm+g\,n_\mp\right).
\end{split}
\end{align}

To obtain frequency of small oscillations one can either linearize these equations or perform real-time evolution of the pseudo-Schr\"odinger equation that appears as a consequence of the inverse Madelung transformation:
\begin{align}\label{pseudopsi}
\I\hbar\partial_t\psi_\pm&= \Big[-\frac{\hbar^2}{2m}\nab^2+\frac{1\hbar^2}{2m}\frac{\nab^2|\psi_\pm|}{|\psi_\pm|}\nn\\
&\quad+A\,|\psi_\pm|^{4/3}+V_{tr}+g |\psi_\mp|^2\Big]\psi_\pm,
\end{align}
Both methods generally yield the same results, and we use either of them, depending on the difficulty of performing the calculations in a given case.

We confirm that hydrodynamics retrieves the TDHF results for radial monopole in both, in- and out-of-phase, cases.
This statement is true for both types of interaction, renormalized and not renormalized.
In the case of other modes, three branches were characterized -- the upper (growing from $\omega / \omega_0 = 2.0$ to $\omega / \omega_0 \approx 2.2$), the middle (decreasing from $\omega / \omega_0 = 2.0$ to $\omega / \omega_0 \approx 1.8$), and the lower one (decreasing from $\omega / \omega_0 = 2.0$ to $\omega / \omega_0 \approx 1.2$).
The first and the last one are retrieved quantitatively with TDHD approach in all cases.
However, the middle branch is always characterized by $\omega / \omega_0 \approx 1.41$, independent of the interaction.
It is not surprising, as such a solution of linearized hydrodynamic equations (in basic form, without renormalization) is explicitly independent from the coupling constant, yielding $\omega / \omega_0 = \sqrt{2}$.

These results strongly suggest that overlapping regions of fermionic clouds can be described hydrodynamically.
It shows that if the hydrodynamic excitation frequency of the noninteracting gas is as it should be (coincidentally, as noninteracting gas is not hydrodynamic), the corrections due to interaction can be evaluated by the means of hydrodynamic description.
It proves to be useful, when the TDHF description becomes intractable -- namely when there are too many atoms in the system.
Therefore, TDHF and TDHD seem to be complementary methods for such a use, TDHF being suitable for small systems, and TDHD being utilized to consider settings closer to experimental setups.

\section{Conclusions}\label{conc}
Recapitulating, we have filled the gap in the literature by analyzing small oscillations of repulsively interacting Fermi-Fermi mixture.
Going beyond previous considerations, we investigated the gas on the repulsive energy branch, in contrast to attractive one of superfluid dimers.
We did not limit ourselves to the paramagnetic phase in which the density profiles of each species are equal, but also covered the phase-separated state.
We calculated monopole and two symmetry-breaking modes by the means of time-dependent Hartree-Fock methods.
Moreover, we compared these results to the hydrodynamic approach, validating the applicability of the latter to investigation of the dynamics of a weakly interacting Fermi gas.
As a future line of work, in the presence of ongoing experiments on quantum mixtures, further investigation of validity of hydrodynamic formalism in such settings can be proposed. 



\paragraph{Funding information}
The work was supported by (Polish) National Science Center Grant 2018/29/B/ST2/01308.

\bibliography{Oscillations.bib}

\nolinenumbers

\end{document}
